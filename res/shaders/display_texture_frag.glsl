#version 130

uniform sampler2D uTex;

in vec2 vTexCoord;
out vec4 fragColor;

void main()
{
    fragColor = texture(uTex, vTexCoord);
}
