#define _USE_MATH_DEFINES

#include <cmath>

#include <Eigen/Eigen>
#include <GLFW/glfw3.h>
#include "imgui.h"
#include "utils.h"

using namespace Eigen;

namespace invLight
{
class Camera
{
private:
    float rx = 0.f, ry = 0.f;
    float maxRx = M_PI / 4;
    float minZ = 2.f;
    float angSpeed = 20.f, zSpeed = 0.2;
    bool cameraUpdates = true;
    int displayW, displayH;
    
    Quaternionf rotation = Quaternionf::Identity();
    
public:
    Camera(GLFWwindow *window)
    {
        glfwGetFramebufferSize(window, &displayW, &displayH);
    }
    ~Camera() { }
    
    Matrix4f getMatrix()
    {
        Matrix3f m3 = rotation.toRotationMatrix();
        Matrix4f m = Matrix4f::Identity();
        m.block<3, 3>(0, 0) = m3;
        m.block<3, 1>(0, 3) = -pos;
        return m;
    }
    
    void drawControls()
    {
        ImGui::Begin("Camera configuration", NULL, ImGuiWindowFlags_AlwaysAutoResize);
        ImGui::SliderFloat("Mouse sensitivity", &angSpeed, 1.f, 100.f, "%.1f");
        ImGui::SliderFloat("Z speed", &zSpeed, 0.01f, 0.5f);
        ImGui::SliderFloat("Minimum Z", &minZ, 0.01f, 10.f);
        ImGui::SliderFloat("Max X angle", &maxRx, 0.f, M_PI / 2);
        ImGui::End();
    }
    
    void update(GLFWwindow* window)
    {
        if(cameraUpdates)
        {
            if(!ImGui::GetIO().WantCaptureKeyboard)
            {
                //if "key" is pressed, glfwGetKey(window, GLFW_KEY_key) = GLFW_PRESSED = 1
                pos(2) += (glfwGetKey(window, GLFW_KEY_DOWN) - glfwGetKey(window, GLFW_KEY_UP)) * zSpeed;
                pos(2) = std::max(pos(2), minZ);
            }
            
            if(!ImGui::GetIO().WantCaptureMouse)
            {
                double mx, my;
                glfwGetCursorPos(window, &mx, &my);
                Vector3f move(mx - displayW / 2, displayH / 2 - my, 0);
                if(move.norm() >= 1)
                {
                    Vector3f dir = move.cross(Vector3f(0, 0, 1));
                    rotation = AngleAxisf(move.norm() * angSpeed * 1e-5, dir) * rotation;
                    rotation.normalize();
                }
            }
            glfwSetCursorPos(window, displayW / 2, displayH / 2);
        }
    }
    
    void freeze(bool doIt)
    {
        cameraUpdates = !doIt;
    }
    
    Vector3f pos = Vector3f(0, 0, 5);
};
}
