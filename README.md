# Real-Time Physically Based Rendering (PBR) Engine
## IGR205 Telecom ParisTech project

A real-time PBR engine built using OpenGL4 by Félix Béasse, Camille Duquennoy,
Mattias Refeyton and Iann Voltz as part of the IGR205 (Project Seminar) class.

### Technical description

This **Physically Based Rendering** engine renders in real-time a GLTF 3D
model by using **High Dynamic Range Image-Based Lighting** as illumination.
It implements the Unreal Engine 4's _split sum approximation_
(based on the Siggraph 2013 article: [Real Shading in Unreal Engine 4](https://cdn2.unrealengine.com/Resources/files/2013SiggraphPresentationsNotes-26915738.pdf)).

The `paintBrush` branch implements a way of painting directly onto the model.


### Building on Windows

Download MSYS2 from the official website ; when that's done, install `git` (to
clone the repo), `gcc` and `make`. Go in the top-level folder and run `make` to
compile or `make run` to compile and run.

### Building on Linux

Install the package `libglfw3-dev`, and run `make` to compile
or `make run` to compile and run.

### Use a specific environment map

To use a specific environment map, add it to the **res** directory (some examples
are available), compile using `make` then run `./inverse_lighting <file name>`
from the **bin** directory.
