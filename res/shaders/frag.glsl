#version 130

uniform sampler2D uAlbedoMap;
uniform sampler2D uMetallicRoughness;
uniform sampler2D uNormalMap;
uniform sampler2D uEmissiveMap;
uniform sampler2D uOcclusionMap;
uniform float uDielectricSpecular; // Dielectric specularity
uniform float L0; // light intensity

uniform sampler2D uIrradianceMap; // precomputed diffuse irradiance, not part of glTF
uniform sampler2D uSpecularMap;
uniform sampler2D uBRDFMap;

uniform vec3 uCameraPos;

in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vPos;
out vec4 fragColor;

const vec3 black = vec3(0.);

/// PBR rendering

vec4 fresnelSchlick(float v, vec4 F0)
{
    return F0 + (1. - F0) * exp2((-5.55473 * v - 6.98316) * v);
}

vec4 fresnelSchlickRoughness(float v, vec4 F0, float roughness)
{
    return F0 + (max(vec4(1.0 - roughness), F0) - F0) * exp2((-5.55473 * v - 6.98316) * v);
}

// GGX (Cook-Torrance) BRDF
vec4 brdf(vec3 l, vec3 v, vec3 n, vec2 metalRough, vec4 baseColor, vec4 F0)
{
    vec3 dielectricSpecular = vec3(uDielectricSpecular);
    vec3 h = normalize(l + v);
    
    float alpha2 = metalRough.g * metalRough.g;
    float k = alpha2 / 2.; // alpha without squaring
    alpha2 *= alpha2;
    float nh = max(0., dot(n, h));
    float nv = max(0., dot(n, v));
    float nl = max(0., dot(n, l));
    float lh = max(0., dot(l, h));
    float vh = max(0., dot(v, h));
    
    // From Epic's Real Shading Paper ; Siggraph 2013
    float temp = nh * nh * (alpha2 - 1.) + 1.;
    float D = alpha2 / (3.1415926 * temp * temp);
    // The G factor differs from Epic's by using L.H and N.H in place of L.N and V.N
    // (possibly a typo from the article)
    float G = lh * nh / ((lh * (1. - k) + k) * (nh * (1. - k) + k));
    vec4 F = fresnelSchlick(vh, F0);
    
    vec4 specular = D * F * G / (4. * nl * nv);
    
    // vec4 diffuse = vec4(mix(baseColor.rgb * (1. - dielectricSpecular.r), black,
    //    metalRough.r), baseColor.a) / 3.1415926;
    vec4 diffuse = baseColor / 3.14159265359;
    
    return (1. - F) * (1. - metalRough.r) * diffuse + specular;
}

/// Normal mapping

// Get the cotangent frame as a 3x3 matrix for tangent space calculations
// From http://www.thetenthplanet.de/archives/1180
mat3 cotangentFrame(vec3 n, vec3 p, vec2 uv)
{
    // Get the triangle's edge vectors and delta UV
    vec3 dp1 = dFdx(p), dp2 = dFdy(p);
    vec2 duv1 = dFdx(uv), duv2 = dFdy(uv);
    
    // Get the tangent and bitangent as covectors
    vec3 dp1perp = cross(n, dp1), dp2perp = cross(dp2, n);
    vec3 t = dp2perp * duv1.x + dp1perp * duv2.x,
        b = dp2perp * duv1.y + dp1perp * duv2.y;
    
    // Construct a scale-invariant frame to avoid determinant calculation
    float invmax = inversesqrt(max(dot(t, t), dot(b, b)));
    return mat3(t * invmax, b * invmax, n);
}

/// Utils

const float PI = 3.14159265359;

// Convert a normalized vector to equirectangular texture coordinates
vec2 norm2equirectangular(vec3 n)
{
    float phi = atan(n.z, n.x + 1e-5);
    float theta = acos(n.y);
    // Use normalized spherical angles as UVs
    return vec2(phi / (2. * PI), theta / PI);
}

void main()
{
    vec3 v = normalize(uCameraPos - vPos);
    vec3 n = normalize(vNormal);
    vec3 normalDisplacement = normalize(texture(uNormalMap, vTexCoord).rgb * 2. - 1.);
    n = normalize(cotangentFrame(n, vPos, vTexCoord) * normalDisplacement);
    vec3 r = reflect(-v, n);
    
    vec4 baseColor = texture(uAlbedoMap, vTexCoord);
    vec2 metalRough = texture(uMetallicRoughness, vTexCoord).bg;
    vec4 F0 = vec4(mix(vec3(uDielectricSpecular), baseColor.rgb, metalRough.r), baseColor.a);
    vec4 emissiveColor = texture(uEmissiveMap, vTexCoord);
    float ao = texture(uOcclusionMap, vTexCoord).r;
    
    float nv = max(dot(n, v), 0.);
    
    // IBL : diffuse irradiance
    vec4 kS = fresnelSchlickRoughness(nv, F0, metalRough.g);
    vec4 kD = (1. - kS) * (1. - metalRough.r);
    vec4 irradiance = vec4(texture(uIrradianceMap, norm2equirectangular(n)).rgb, 1.);
    // Specular irradiance
    const float MAX_REFLECTION_LOD = 4.;
    vec4 prefiltered = textureLod(uSpecularMap, norm2equirectangular(n), metalRough.g * MAX_REFLECTION_LOD);
    // BRDF
    vec2 envBRDF = texture(uBRDFMap, vec2(nv, metalRough.g)).rg;
    vec4 specular = prefiltered * (kS * envBRDF.x + envBRDF.y);
    
    vec4 ambient = (irradiance * baseColor * kD + specular);
    
    vec4 color = emissiveColor + ao * ambient + L0 * brdf(v, v, n, metalRough, baseColor, F0) * nv;
    color = color / (color + 1.);
    fragColor = vec4(color.rgb, 1.);
}
