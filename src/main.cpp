#define GLFW_DLL
// #define TINYGLTF_NOEXCEPTION // optional. disable exception handling.

#include <iostream>
#include <map>
#include <string>

#include <Eigen/Eigen>
#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Camera.h"
// Define these only in *one* .cpp file.
#include "GltfModel.h"
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "tiny_gltf.h"
#include "utils.h"

using namespace Eigen;
using namespace tinygltf;

invLight::Camera *camera;
Vector4f brushColor(0, 0, 0, 1);
float brushSize = 1;

// Need to resize
float quad_vertices[] =
{
    -1.f, -1.f, 1.f,
    1.f, -1.f, 1.f,
    1.f, 1.f, 1.f,
    -1.f, 1.f, 1.f
};

unsigned int quad_indices[] =
{
    0, 1, 2,
    0, 2, 3
};

static void glfw_error_callback(int error, const char *description)
{
    std::cerr << "Error " << error << " : " << description << std::endl;
}

// Expects an identity matrix as input
void perspective(Matrix4f &p, float fov, float ratio, float near, float far)
{
	float d = 1 / tan(fov * M_PI / 180 / 2);
	float ir = 1. / (near - far);

    p(0, 0) = d;
    p(1, 1) = d * ratio;
    p(2, 2) = (near + far) * ir;
    p(3, 3) = 0;
    p(3, 2) = -1;
    p(2, 3) = 2 * near * far * ir;
}

inline void setAspectRatio(Matrix4f &p, float ratio)
{
    p(1, 1) = p(0, 0) * ratio;
}

/*
d,               0,                   0,                   0,
0, d * aspectRatio,                   0,                   0,
0,               0,   (near + far) * ir, 2 * near * far * ir,
0,               0,                  -1,                   0
*/

void basicInterface()
{
    ImGui::Begin("Basic interface", NULL, ImGuiWindowFlags_AlwaysAutoResize);
    ImGui::ColorPicker4("Brush color", &brushColor(0), ImGuiColorEditFlags_RGB, NULL);
    ImGui::SliderFloat("Brush size", &brushSize, 0.1f, 10.f);
    ImGui::End();
}

void printShaderLog(GLuint shader)
{
    GLint logSize;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
    if(logSize > 0)
    {
        char *log = new char[logSize];
        glGetShaderInfoLog(shader, logSize, &logSize, log);
        std::cerr << "Log for shader #" << shader << " :" << std::endl;
        std::cerr << log << std::endl;
        delete[] log;
    }
    else
        std::cerr << "Shader #" << shader << " has no log" << std::endl;
}

void keyCallback(GLFWwindow *window, int key, int scanCode, int action, int mods)
{
    ImGui_ImplGlfw_KeyCallback(window, key, scanCode, action, mods);
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        camera->freeze(true);
    }
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
    if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && !ImGui::GetIO().WantCaptureMouse)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        camera->freeze(false);
    }
}

/**
 * Returns a texture identifier filled with a precomputed irradiance map calculated
 * from a provided, already set up-environment map
 */
GLuint precomputeIrradianceMap(GLuint envMapId)
{
    std::cerr << "Precomputing irradiance map ... " << std::endl;

    GLuint irradianceVAO, irradianceVBO, irradianceMap,
        irradianceVertShader, irradianceFragShader, irradianceProgram, captureFBO,
        captureRBO;
    GLint aPos_location, env_map;

    glGenVertexArrays(1, &irradianceVAO);
    glBindVertexArray(irradianceVAO);
    glGenBuffers(1, &irradianceVBO);
    glBindBuffer(GL_ARRAY_BUFFER, irradianceVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertices), quad_vertices, GL_STATIC_DRAW);

    glGenFramebuffers(1, &captureFBO);
    glGenRenderbuffers(1, &captureRBO);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 320, 320);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    checkGLerror();

    glGenTextures(1, &irradianceMap);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, irradianceMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 320, 320, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_2D, envMapId);

    checkGLerror();

    irradianceVertShader = createShaderFromSource(GL_VERTEX_SHADER, "shaders/irradiance_vert.glsl");
    irradianceFragShader = createShaderFromSource(GL_FRAGMENT_SHADER, "shaders/irradiance_frag.glsl");

    irradianceProgram = glCreateProgram();
    glAttachShader(irradianceProgram, irradianceVertShader);
    glAttachShader(irradianceProgram, irradianceFragShader);
    printShaderLog(irradianceVertShader);
    printShaderLog(irradianceFragShader);
    glLinkProgram(irradianceProgram);

    glUseProgram(irradianceProgram);

    checkGLerror();

    env_map = glGetUniformLocation(irradianceProgram, "environmentMap");
    aPos_location = glGetAttribLocation(irradianceProgram, "aPos");
    glEnableVertexAttribArray(aPos_location);
    checkGLerror();
    glVertexAttribPointer(aPos_location, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    glUniform1i(env_map, 0);

    checkGLerror();

    glViewport(0, 0, 320, 320);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, irradianceMap, 0);

    checkGLerror();

    glClearDepth(1.f);
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    checkGLerror();

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, quad_indices);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    checkGLerror();

    // glDisableVertexAttribArray(aPos_location);
    glDeleteShader(irradianceVertShader);
    glDeleteShader(irradianceFragShader);
    glDeleteProgram(irradianceProgram);
    glDeleteFramebuffers(1, &captureFBO);
    glDeleteRenderbuffers(1, &captureRBO);
    glDeleteBuffers(1, &irradianceVBO);
    glDeleteVertexArrays(1, &irradianceVAO);

    checkGLerror();

    std::cerr << "... done " << std::endl;

    return irradianceMap;
}

GLuint precomputeSpecularMap(GLuint envMapId, unsigned int maxMipLevels)
{
    std::cerr << "Precomputing specular map ... " << std::endl;

    GLuint specularMap, prefilterSpecularVAO, prefilterSpecularVBO,
        prefilterSpecularVertShader, prefilterSpecularFragShader,
        prefilterSpecularProgram, captureFBO, captureRBO;
    GLint aPos_location, env_map, roughness_location, resolution_location;

    prefilterSpecularVertShader = createShaderFromSource(GL_VERTEX_SHADER, "shaders/prefilter_specular_vert.glsl");
    prefilterSpecularFragShader = createShaderFromSource(GL_FRAGMENT_SHADER, "shaders/prefilter_specular_frag.glsl");

    prefilterSpecularProgram = glCreateProgram();
    glAttachShader(prefilterSpecularProgram, prefilterSpecularVertShader);
    glAttachShader(prefilterSpecularProgram, prefilterSpecularFragShader);
    printShaderLog(prefilterSpecularVertShader);
    printShaderLog(prefilterSpecularFragShader);
    glLinkProgram(prefilterSpecularProgram);

    glUseProgram(prefilterSpecularProgram);

    checkGLerror();

    glGenVertexArrays(1, &prefilterSpecularVAO);
    glBindVertexArray(prefilterSpecularVAO);
    glGenBuffers(1, &prefilterSpecularVBO);
    glBindBuffer(GL_ARRAY_BUFFER, prefilterSpecularVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertices), quad_vertices, GL_STATIC_DRAW);

    glGenFramebuffers(1, &captureFBO);
    glGenRenderbuffers(1, &captureRBO);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);

    glGenTextures(1, &specularMap);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, specularMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 512, 512, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, envMapId);

    checkGLerror();

    unsigned int mipWidth = 512;
    unsigned int mipHeight = 512;
    for (unsigned int mipLevel = 0; mipLevel < maxMipLevels; mipLevel++)
    {
        glViewport(0, 0, mipWidth, mipHeight);
        glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
        glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);

        std::cerr << __FILE__ << ":" << __LINE__ << " framebuffer status : " << glCheckFramebufferStatus(GL_FRAMEBUFFER) << std::endl;
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipWidth, mipHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);
        std::cerr << __FILE__ << ":" << __LINE__ << " framebuffer status : " << glCheckFramebufferStatus(GL_FRAMEBUFFER) << std::endl;

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, specularMap, mipLevel);

        std::cerr << __FILE__ << ":" << __LINE__ << " framebuffer status : " << glCheckFramebufferStatus(GL_FRAMEBUFFER) << std::endl;

        checkGLerror();

        env_map = glGetUniformLocation(prefilterSpecularProgram, "environmentMap");
        roughness_location = glGetUniformLocation(prefilterSpecularProgram, "roughness");
        resolution_location = glGetUniformLocation(prefilterSpecularProgram, "resolution");
        aPos_location = glGetAttribLocation(prefilterSpecularProgram, "aPos");
        glEnableVertexAttribArray(aPos_location);
        checkGLerror();
        glVertexAttribPointer(aPos_location, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

        glUniform1i(env_map, 0);

        float roughness = (float)mipLevel / (float)(maxMipLevels - 1);
        glUniform1f(roughness_location, roughness);
        glUniform1f(resolution_location, 512);

        checkGLerror();

        glClearDepth(1.f);
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        checkGLerror();

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, quad_indices);
        mipWidth /= 2;
        mipHeight /= 2;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    checkGLerror();

    glDisableVertexAttribArray(aPos_location);
    glDeleteShader(prefilterSpecularVertShader);
    glDeleteShader(prefilterSpecularFragShader);
    glDeleteProgram(prefilterSpecularProgram);
    glDeleteFramebuffers(1, &captureFBO);
    glDeleteRenderbuffers(1, &captureRBO);
    glDeleteBuffers(1, &prefilterSpecularVBO);
    glDeleteVertexArrays(1, &prefilterSpecularVAO);

    std::cerr << "... done " << std::endl;

    return specularMap;
}

GLuint precomputeBRDFMap()
{
    std::cerr << "Precomputing BRDF map ... " << std::endl;

    GLuint brdfVAO, brdfVBO, brdfMap,
        brdfVertShader, brdfFragShader, brdfProgram, captureFBO,
        captureRBO;
    GLint aPos_location;

    glGenVertexArrays(1, &brdfVAO);
    glBindVertexArray(brdfVAO);
    glGenBuffers(1, &brdfVBO);
    glBindBuffer(GL_ARRAY_BUFFER, brdfVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertices), quad_vertices, GL_STATIC_DRAW);

    glGenFramebuffers(1, &captureFBO);
    glGenRenderbuffers(1, &captureRBO);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    checkGLerror();

    glGenTextures(1, &brdfMap);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, brdfMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    checkGLerror();

    brdfVertShader = createShaderFromSource(GL_VERTEX_SHADER, "shaders/brdf_vert.glsl");
    brdfFragShader = createShaderFromSource(GL_FRAGMENT_SHADER, "shaders/brdf_frag.glsl");

    brdfProgram = glCreateProgram();
    glAttachShader(brdfProgram, brdfVertShader);
    glAttachShader(brdfProgram, brdfFragShader);
    printShaderLog(brdfVertShader);
    printShaderLog(brdfFragShader);
    glLinkProgram(brdfProgram);

    glUseProgram(brdfProgram);

    checkGLerror();

    aPos_location = glGetAttribLocation(brdfProgram, "aPos");
    glEnableVertexAttribArray(aPos_location);
    checkGLerror();
    glVertexAttribPointer(aPos_location, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    checkGLerror();

    glViewport(0, 0, 512, 512);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, brdfMap, 0);

    checkGLerror();

    glClearDepth(1.f);
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    checkGLerror();

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, quad_indices);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    checkGLerror();

    // glDisableVertexAttribArray(aPos_location);
    glDeleteShader(brdfVertShader);
    glDeleteShader(brdfFragShader);
    glDeleteProgram(brdfProgram);
    glDeleteFramebuffers(1, &captureFBO);
    glDeleteRenderbuffers(1, &captureRBO);
    glDeleteBuffers(1, &brdfVBO);
    glDeleteVertexArrays(1, &brdfVAO);

    checkGLerror();

    std::cerr << "... done " << std::endl;

    return brdfMap;
}

int main(int argc, char *argv[])
{
    setwd(argv);

    // Geometry initialization
    GLuint vertex_array_objs[2], vertex_shader, fragment_shader, program, irradianceTexture,
        specularTexture, brdfTexture;
    GLuint skybox_vertex_buffer, skybox_vertex_shader, skybox_fragment_shader,
        skybox_program, skybox_texture;
    GLint mv_location, p_location, uDS_location, L0_location, irradiance_location,
        specular_location, brdf_location, cameraPos_location;
    GLint skybox_v_location, skybox_p_location, skybox_aPos_location, skybox_location;

    const char* filename = "environment.hdr";

    if (argc == 2)
      filename = argv[1];
    
    int skyboxX, skyboxY, skyboxComponentsInFile;
    const float *skybox = stbi_loadf(filename, &skyboxX, &skyboxY, &skyboxComponentsInFile, 4);
    if(!skybox)
    {
        std::cerr << "Couldn't load " << filename << std::endl;
        return 1;
    }

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
    {
        std::cerr << "Couldn't initialize GLFW" << std::endl;
        return 1;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    GLFWwindow *window = glfwCreateWindow(1280, 720, "Inverse Lighting", NULL, NULL);
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    glfwSwapInterval(1); // Enable vsync
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Setup ImGui binding
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls
    ImGui_ImplGlfwGL3_Init(window, true);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);

    // Setup style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // First of all, compute the irradiance map
    glGenTextures(1, &skybox_texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, skybox_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, skyboxX, skyboxY, 0, GL_RGBA, GL_FLOAT, (const GLvoid *)skybox);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    irradianceTexture = precomputeIrradianceMap(skybox_texture);
    specularTexture = precomputeSpecularMap(skybox_texture, 5);
    brdfTexture = precomputeBRDFMap();

    /* For the prefilterSpecularTexture, make a "for" loop on the number of mip-levels/roughness-levels,
    with roughness = mipLevel / (maxMipLevels - 1) */

    glGenVertexArrays(2, vertex_array_objs);
    // Then, load the skybox and precompute the irradiance map
    glBindVertexArray(vertex_array_objs[0]);
    glGenBuffers(1, &skybox_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, skybox_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertices), quad_vertices, GL_STATIC_DRAW);

    skybox_vertex_shader = createShaderFromSource(GL_VERTEX_SHADER, "shaders/skybox_vert.glsl");
    skybox_fragment_shader = createShaderFromSource(GL_FRAGMENT_SHADER, "shaders/skybox_frag.glsl");

    skybox_program = glCreateProgram();
    glAttachShader(skybox_program, skybox_vertex_shader);
    printShaderLog(skybox_vertex_shader);
    glAttachShader(skybox_program, skybox_fragment_shader);
    printShaderLog(skybox_fragment_shader);
    glLinkProgram(skybox_program);

    skybox_v_location = glGetUniformLocation(skybox_program, "invV");
    skybox_p_location = glGetUniformLocation(skybox_program, "invP");
    skybox_aPos_location = glGetAttribLocation(skybox_program, "aPos");
    skybox_location = glGetUniformLocation(skybox_program, "skybox");

    glEnableVertexAttribArray(skybox_aPos_location);
    glVertexAttribPointer(skybox_aPos_location, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3,
        (void *) 0);

    glUseProgram(skybox_program);
    glUniform1i(skybox_location, 0);

    // Finally, GLTF initialization

    invLight::GltfModel model;
    TinyGLTF loader;
    std::string err;
    camera = new invLight::Camera(window);

    bool ret = loader.LoadASCIIFromFile(&model, &err, "DamagedHelmet/DamagedHelmet.gltf");
    // bool ret = loader.LoadBinaryFromFile(&model, &err, argv[1]); // for binary glTF(.glb)
    if (!err.empty())
        std::cerr << "Err: " << err << std::endl;
    if (!ret)
    {
        std::cerr << "Failed to parse glTF" << std::endl;
        return -1;
    }

    glBindVertexArray(vertex_array_objs[1]);

    model.initForRendering();

    vertex_shader = createShaderFromSource(GL_VERTEX_SHADER, "shaders/vert.glsl");
    fragment_shader = createShaderFromSource(GL_FRAGMENT_SHADER, "shaders/frag.glsl");

    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    printShaderLog(vertex_shader);
    glAttachShader(program, fragment_shader);
    printShaderLog(fragment_shader);
    glLinkProgram(program);

    checkGLerror();

    std::cerr << "Arming glTF model for rendering" << std::endl;
    auto vertexAttribs = model.armForRendering(program);
    std::cerr << "Done arming" << std::endl;

    mv_location = glGetUniformLocation(program, "MV");
    p_location = glGetUniformLocation(program, "P");
    uDS_location = glGetUniformLocation(program, "uDielectricSpecular");
    L0_location = glGetUniformLocation(program, "L0");
    irradiance_location = glGetUniformLocation(program, "uIrradianceMap");
    specular_location = glGetUniformLocation(program, "uSpecularMap");
    brdf_location = glGetUniformLocation(program, "uBRDFMap");
    cameraPos_location = glGetUniformLocation(program, "uCameraPos");

    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);

    Matrix4f p = Matrix4f::Identity();
    perspective(p, 90, (float)display_w / display_h, 0.1, 10);
    Matrix4f invP = p.inverse();
    glUseProgram(program);
    glUniformMatrix4fv(p_location, 1, GL_FALSE, (const GLfloat *)p.data());
    glUniform3f(cameraPos_location, camera->pos(0), camera->pos(1), camera->pos(2));

    checkGLerror();

    std::cerr << "Drawing " << model.indicesCount() << " indices" << std::endl;

    glViewport(0, 0, display_w, display_h);

    glClearDepth(1.f);
    glClearColor(0.45f, 0.55f, 0.60f, 1.00f);

    // Dielectric specularity (GGX)
    float ds = 0.04f, l0 = 1.f;

    bool skyOrIrradiance = false, debugDraw = false;

    std::cerr << "Entering drawing loop" << std::endl;

    float ratio = (float)display_w / display_h;

    while (!glfwWindowShouldClose(window))
    {
        glfwGetFramebufferSize(window, &display_w, &display_h);
        float newRatio = (float)display_w / display_h;
        if(ratio != newRatio)
        {
            ratio = newRatio;
            setAspectRatio(p, ratio);
            invP = p.inverse();
            glViewport(0, 0, display_w, display_h);
        }

        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        ImGui_ImplGlfwGL3_NewFrame();
        // ImGui::ShowDemoWindow();

        // Rendering + OpenGL rendering
        // Draw the skybox, then the model
        ImGui::Begin("Physical parameters", NULL, ImGuiWindowFlags_AlwaysAutoResize);
        ImGui::SliderFloat("Dielectric specularity", &ds, 0.f, 1.f, "%.3f");
        ImGui::SliderFloat("Light intensity", &l0, 0.f, 10.f, "%.2f");
        ImGui::Checkbox("Use irradiance map as skybox", &skyOrIrradiance);
        ImGui::Checkbox("Debug draw irradiance map", &debugDraw);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, skyOrIrradiance ? specularTexture : skybox_texture);
        ImGui::End();
        Matrix4f v = camera->getMatrix(), invV = v.inverse();

        // glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(vertex_array_objs[0]);
        glUseProgram(skybox_program);
        glUniformMatrix4fv(skybox_v_location, 1, GL_FALSE, (const GLfloat *) invV.data());
        glUniformMatrix4fv(skybox_p_location, 1, GL_FALSE, (const GLfloat *) invP.data());
        glDisable(GL_DEPTH_TEST);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, quad_indices);

        glBindVertexArray(vertex_array_objs[1]);
        glUseProgram(program);

        glUniformMatrix4fv(mv_location, 1, GL_FALSE, (const GLfloat *) v.data());
        glUniformMatrix4fv(p_location, 1, GL_FALSE, (const GLfloat *)p.data());
        glUniform1f(uDS_location, ds);
        glUniform1f(L0_location, l0);
        // Diffuse irradiance
        glActiveTexture(GL_TEXTURE0 + model.activeTexturesCount());
        glBindTexture(GL_TEXTURE_2D, irradianceTexture);
        glUniform1i(irradiance_location, model.activeTexturesCount());
        // Specular irradiance
        glActiveTexture(GL_TEXTURE0 + model.activeTexturesCount() + 1);
        glBindTexture(GL_TEXTURE_2D, specularTexture);
        glUniform1i(specular_location, model.activeTexturesCount() + 1);
        // BRDF
        glActiveTexture(GL_TEXTURE0 + model.activeTexturesCount() + 2);
        glBindTexture(GL_TEXTURE_2D, brdfTexture);
        glUniform1i(brdf_location, model.activeTexturesCount() + 2);

        glEnable(GL_DEPTH_TEST);
        model.render();

        if(debugDraw)
        {
            displayTexture(specularTexture);
            displayTexture(skybox_texture, 0.f, -1.f);
        }

        camera->drawControls();
        // Draw basic interface
        basicInterface();

        ImGui::Render();
        ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
        glfwPollEvents();
        camera->update(window);
    }

    std::cerr << "Exiting drawing loop" << std::endl;

    glDeleteTextures(1, &brdfTexture);
    glDeleteTextures(1, &specularTexture);
    glDeleteTextures(1, &irradianceTexture);
    glDeleteTextures(1, &skybox_texture);

    glDeleteProgram(skybox_program);
    glDeleteProgram(program);
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    glDeleteShader(skybox_vertex_shader);
    glDeleteShader(skybox_fragment_shader);

    glDeleteVertexArrays(2, vertex_array_objs);
    glDeleteBuffers(1, &skybox_vertex_buffer);

    model.cleanup();

    delete camera;

    // Cleanup
    ImGui_ImplGlfwGL3_Shutdown();
    ImGui::DestroyContext();
    glfwTerminate();

    return 0;
}
