#version 130

uniform sampler2D environmentMap;

in vec2 dirSpherical;
out vec4 fragColor;

const float PI = 3.14159265359;

void main()
{
    vec3 irradiance = vec3(0.0);
    
    // Convolution code
    float sampleDelta = 0.025;
    int nrSamples = 0;
    
    for (float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
    {
        for (float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
        {
            float t = theta - 0.25 * PI + dirSpherical.y;
            // Spherical theta cannot be greater than PI
            // Shift the spherical coordinates if needed
            // This is done by adding PI to phi and subtracting theta from 2 PI
            // Note : the phi expression for the "no shift" scenario is phi - PI
            vec2 sampleVec = t > PI ? vec2(phi + dirSpherical.x, 2. * PI - t) : vec2(phi - PI + dirSpherical.x, t);
            irradiance += texture(environmentMap, sampleVec / vec2(PI * 2., PI)).rgb * cos(theta) * sin(theta);
            nrSamples++;
        }
    }
    irradiance *= PI / float(nrSamples);
    
    fragColor = vec4(irradiance, 1.0);
}
