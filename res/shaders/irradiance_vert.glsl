#version 130

in vec3 aPos;
out vec2 dirSpherical;

const float PI = 3.14159265359;

void main()
{
    // Map [-1, 1]^2 to spherical phi, theta
    dirSpherical = (aPos.xy + 1.) * vec2(PI, PI / 2.);
    gl_Position = vec4(aPos, 1.);
}
