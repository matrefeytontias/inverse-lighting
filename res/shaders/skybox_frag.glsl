#version 330

uniform sampler2D skybox;   //Need to use HDR environment

in vec3 vRayDir;
out vec4 fragColor;

void main()
{
    vec3 rayDir = normalize(vRayDir);
    
    // Spherical coordinates change
    float theta = atan(rayDir.z, rayDir.x);
    float phi = asin(rayDir.y);
    // Use normalized spherical angles as UVs
    vec2 skyboxCoord = vec2(theta / (2. * 3.141592) + 0.5, 0.5 - phi / 3.1415926);
    
    fragColor = texture2D(skybox, skyboxCoord);
}
