#version 130

in vec3 aPos;
out vec2 vTexCoord;

void main()
{
    vTexCoord = (vTexCoord.xy + 1.) / 2.;
    gl_Position = vec4(aPos, 1.);
}
