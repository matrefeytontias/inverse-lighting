#version 140

uniform mat4 MV;
uniform mat4 P;

uniform vec3 uCameraPos;

in vec3 POSITION;
in vec3 NORMAL;
in vec2 TEXCOORD_0;

out vec3 vNormal;
out vec3 vPos;
out vec2 vTexCoord;

void main()
{
    vTexCoord = TEXCOORD_0;
    vNormal = mat3(MV) * NORMAL;
    vPos = mat3(MV) * POSITION;
    gl_Position = P * MV * vec4(POSITION, 1.);
}
