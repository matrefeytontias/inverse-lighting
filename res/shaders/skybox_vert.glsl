#version 330

uniform mat4 invV;
uniform mat4 invP;

in vec3 aPos;
out vec3 vRayDir;

void main()
{
    vRayDir = (invV * invP * vec4(aPos, 1.)).xyz;
    
    gl_Position = vec4(aPos, 1.0);
}
